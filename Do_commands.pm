package Do_commands;
use Carp qw(croak); 
#REQUIRES: A list of commands, with optional description for debug purposes.
#          Format for this list must be a 2D array
#EFFECTS:  Executes commands in the order of the input array, prints debug
#          if $debug flag is set to 1
#NOTES: your commands array must be passed as an array reference, like so:
#       command_ary => \@my_array
#       The format of the array is ["command executed inside backticks", "description of command"]
sub do_commands{
  my %arg_of;
  %arg_of = @_ if (@_);
  #constants:
  my $CMD_COL = 0;
  my $DESCRIPTION_COL = 1;

  my $timestamp;
  #function argument variables:
  my ($debug, $commands_ref);

  #--- begin arg checking ---
  if(exists $arg_of{command_ary}){
          $commands_ref = $arg_of{command_ary};


          unless($commands_ref =~ /\S/){
                  croak "ERROR: Empty command_ary argument in do_commands\n";
          }
  }
  else{
          #default to current directory
          croak "ERROR: Must supply array of commands to do_commands. Exiting\n";
  }
  #Check if debug is supplied
  if(exists $arg_of{debug}){
          $debug = $arg_of{debug};
          unless($debug  =~ /\S/){
                  croak "ERROR: debug passed to do_commands as empty string\n";
          }
  }
  else{
    $debug = $DISABLE
  }

  my $cmd_output = "";

  my @commands = @{$commands_ref};
  my ($command, $details);

  my $num_commands = scalar(@commands);
  my $i;
  for($i=0; $i < $num_commands; $i++){
    #execute command


    $cmd_output = `$commands[$i][$CMD_COL]`;
    #print debug messages if flag is set
    if($debug){
      print "\n";
      $timestamp = localtime();
      print  "[$timestamp] " . $commands[$i][$DESCRIPTION_COL];
      #print "\n" . $cmd_output . "..done\n";
    }
  }

} #END sub do_commands()


1; #Packages need to return true
