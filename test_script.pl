#!/usr/bin/perl -w
use warnings;
use File::Basename;
#use local dir as a possible source for pm files
use Cwd qw(abs_path);
use lib dirname (abs_path(__FILE__));

require Do_commands;
use Do_commands;

my $create_temp = "touch my.tmp";

my @cmds = (
	["mkdir tmp", "make temporary directory"],
	["touch tmp/my.tmp", "make a temp file"]
);

Do_commands::do_commands(debug => 1, command_ary => \@cmds);
